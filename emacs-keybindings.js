console.log("Emacs keybindings v1.0 loaded.");

document.getElementsByTagName("BODY")[0].focus();

const ControlKeyBindings = {
    'n' : () => window.scrollBy(50, 0),
    'p' : () => window.scrollBy(-50, 0),
    'x' : () => confirm('Close tab?') ? console.log('closing') : console.log('staying'),
    // 'v' : () => console.log('scroll forward'),
}

const MetaKeyBindings = {
    '>' : () => window.scrollTo(0, document.body.scrollHeight),
    '<' : () => window.scrollTo({top: 0}),
    // 'v' : console.log('scroll backwards'),
};


window.addEventListener('load', function ()  {
    document.addEventListener('keydown', function (e) {
	// console.log(e.key);
	
	if(e.ctrlKey){
    	    ControlKeyBindings[e.key]();
	}
	
	if(e.altKey){
	    if(e.ShiftKey){
		MetaKeyBindings[e.key]();
	    }
	    MetaKeyBindings[e.key]();
	}
    }, true);
}, false);


// keyBindings['Meta+<']();
