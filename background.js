function openPage() {
  browser.tabs.create({
    url: "https://www.gnu.org/software/emacs/"
  });
}

browser.browserAction.onClicked.addListener(openPage);
